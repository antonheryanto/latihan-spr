﻿using Latihan.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Latihan
{
    public class Current
    {
        public static Db Db
        {
            get
            {
                var db = HttpContext.Current.Items["db"] as Db;
                if (db != null) return db;
                var cs = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
                var cn = new SqlConnection(cs);
                cn.Open();
                var p = new StackExchange.Profiling.Data.ProfiledDbConnection(
                    cn, StackExchange.Profiling.MiniProfiler.Current);
                db = Db.Init(p, 30);
                HttpContext.Current.Items["db"] = db;
                return db;
            }
        }

        public static void DisposeDb()
        {
            var db = HttpContext.Current.Items["db"] as Db;
            if (db == null) return;
            db.Dispose();
            HttpContext.Current.Items["db"] = null;
        }
    }
}