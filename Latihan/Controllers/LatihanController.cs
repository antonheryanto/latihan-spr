﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Latihan.Controllers
{
    [Authorize]
    public class LatihanController : Controller
    {
        //
        // GET: /Latihan/
        public ActionResult Index()
        {
            var m = Current.Db.Latihan.All();
            return View(m);
        }

        //
        // GET: /Latihan/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Latihan/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Latihan/Create
        [HttpPost]
        public ActionResult Create(Models.Latihan m)
        {
            try
            {
                Current.Db.Latihan.Insert(new { m.Nama, m.Anjuran, m.BilHari });

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Latihan/Edit/5
        public ActionResult Edit(int id)
        {
            var m = Current.Db.Latihan.Get(id);
            return View(m);
        }

        //
        // POST: /Latihan/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Latihan.Models.Latihan m)
        {
            try
            {
                // TODO: Add update logic here
                Current.Db.Latihan.Update(id, new { m.Nama, m.Anjuran, m.BilHari });
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Latihan/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Latihan/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
