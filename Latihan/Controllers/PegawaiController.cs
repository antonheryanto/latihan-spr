﻿using Latihan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Latihan.Controllers
{
    public class PegawaiController : Controller
    {
        //
        // GET: /Pegawai/
        public ActionResult Index()
        {
            //var m = Current.Db.Pegawai.All();
            return View();
        }

        //
        // GET: /Pegawai/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Pegawai/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Pegawai/Create
        [HttpPost]
        public ActionResult Create(Pegawai m)
        {
            try
            {
                // TODO: Add insert logic here
                Current.Db.Pegawai.Insert(new { IdPegawai = m.Id, IdLatihan = m.Ic });
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        //
        // GET: /Pegawai/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Pegawai/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Pegawai/Delete/5
        public ActionResult Delete(int id)
        {
            Current.Db.Pegawai.Delete(id);
            return Content("ok");
        }

        //
        // POST: /Pegawai/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
