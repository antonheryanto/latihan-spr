﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Latihan.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            ViewBag.Pengguna = this.PenggunaSaje;
            return View();
        }

        [Route("admin/errors/{resource?}/{subResource?}")]
        public ActionResult InvokeErrorHandler(string resource, string subResource)
        {
            var context = System.Web.HttpContext.Current;
            var factory = new StackExchange.Exceptional.HandlerFactory();

            var page = factory.GetHandler(context, Request.RequestType, Request.Url.ToString(), Request.PathInfo);
            page.ProcessRequest(context);

            return null;
        }
	}
}