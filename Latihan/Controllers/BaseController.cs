﻿using Latihan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Latihan.Controllers
{
    public class BaseController : Controller
    {
        Pengguna pengguna;
        public Pengguna PenggunaSaje { get { return pengguna = pengguna ?? InitPeguna(); } }

        Pengguna InitPeguna()
        {
            if (!Request.IsAuthenticated) return null;
            int id;
            if (!int.TryParse(User.Identity.Name, out id) || id == 0)
            {
                FormsAuthentication.SignOut();
                return null;
            }
            var u = Current.Db.Pegguna.Get(id);
            return u;
        }


	}
}