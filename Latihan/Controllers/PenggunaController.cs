﻿using Latihan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Latihan.Controllers
{
    public class PenggunaController : Controller
    {
        //
        // GET: /Pengguna/
        [Authorize]
        public ActionResult Index()
        {
            var m = Current.Db.Pegguna.All();
            return View(m);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Pegawai m)
        {
            if (string.IsNullOrWhiteSpace(m.Emel))
            {
                ModelState.AddModelError("emel", "oi mana emel nih");
            }
            m.Katalaluan = m.Katalaluan.ToMD5();
            var id = Current.Db.Pegguna.Insert(new { m.Emel, m.Katalaluan, m.Nama });

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Pengguna m)
        {
            var kl = m.Katalaluan.ToMD5();
            var u = Current.Db.Query<Pengguna>(@"select id,adminKan from [Pengguna] 
                where emel=@emel AND katalaluan=@kl", new { m.Emel, kl }).FirstOrDefault();
            if (u == null) {
                ModelState.AddModelError("Emel", "Emel atau Katalaluan silap tau!");
                return View(m);
            }
            var role = u.AdminKan ? "Admin" : "";
            var ticket = new FormsAuthenticationTicket(
                1,
                u.Id.ToString(),
                DateTime.Now,
                DateTime.Now.AddHours(2),
                true,
                role
            );
            var encryptedTicket = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.Expires = ticket.Expiration;
            Response.Cookies.Add(cookie);
            return Redirect("~/");
        }

        public ActionResult Keluar()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult Delete(int id)
        {
            Current.Db.Pegguna.Delete(id);
            return Content("ok");
        }
	}
}