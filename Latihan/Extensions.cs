﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Latihan
{
    public static class Extensions
    {
        public static string ToMD5(this string input)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            var inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            var hashBytes = md5.ComputeHash(inputBytes);
            var b = new System.Text.StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                b.Append(hashBytes[i].ToString("X2"));
            }
            return b.ToString();
        }
    }
}