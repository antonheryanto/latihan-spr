﻿using System;

namespace Latihan.Models
{
    public class Pegawai : Pengguna
    {
        public int Ic { get; set; }
        public DateTime? TarikhJawatan { get; set; }
    }
}