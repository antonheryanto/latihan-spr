﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Latihan.Models
{
    public class Db : Dapper.Database<Db>
    {
        public Table<Pengguna> Pegguna { get; set; }
        public Table<Pegawai> Pegawai { get; set; }
        public Table<Latihan> Latihan { get; set; }
        public Table<LatihanPegawai> LatihanPegawai { get; set; }
    }
}