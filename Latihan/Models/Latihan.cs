﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Latihan.Models
{
    public class Latihan
    {
        public int Id { get; set; }
        [Required]
        public string Nama { get; set; }
        [Required]
        public string Anjuran { get; set; }
        public DateTime? TarikhMula { get; set; }
        public DateTime? TarikhAkhir { get; set; }
        public int BilHari { get; set; }
    }
}