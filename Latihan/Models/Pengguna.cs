﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Latihan.Models
{
    public class Pengguna
    {
        public int Id { get; set; }
        public string Emel { get; set; }
        [DataType(DataType.Password)]
        public string Katalaluan { get; set; }
        public string Nama { get; set; }
        public bool AktifKan { get; set; }
        public bool AdminKan { get; set; }
        public DateTime? Wujud { get; set; }
        public DateTime? Ubah { get; set; }
    }
}