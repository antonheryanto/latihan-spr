﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Latihan.Models
{
    public class LatihanPegawai
    {
        public int IdLatihan { get; set; }
        public int IdPegawai { get; set; }
        public string Sijil { get; set; }
    }
}